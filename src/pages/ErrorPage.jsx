import React from 'react'
import styled from 'styled-components';
import Header from '../components/Header';
import AppNavBar from '../components/AppNavBar';


const Contain = styled.div``

const Container = styled.div``

const Title = styled.h1``

const ErrorPage = () => {
	return (
		<Contain>
		<Header />
		<AppNavBar />
			<Container>
				<Title>
					Error Page
				</Title>
			</Container>
		</Contain>
	)
}

export default ErrorPage;