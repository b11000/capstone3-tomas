import 
	React, { 
	useState,
	useEffect,
	useContext,
	// createContext
} from 'react';
// import UserContext from '../userContext'
import { 
	Redirect,
	// Navigate,
	useHistory
 } from 'react-router-dom';
import Swal from "sweetalert2";
import styled from 'styled-components';
import Header from '../components/Header';
import AppNavBar from '../components/AppNavBar';
import UserContext from '../UserContext';

const Container = styled.div`
	width: 100vw;
	height: 100vh;
	// background: linear-gradient(
	// 	rgba(255,255,255,0.5),
	// 	rgba(255,255,255,0.5)
	// 	), url("https://picsum.photos/1000/1000")center;
	background-color: #1b1b1b;
	background-size: cover;    
	display: flex;
	align-items : center;
	justify-content: center;
	`;
	
const Wrapper = styled.div`
	width: 40%;
	padding: 20px;
	background-color: white;
	`;
	
const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
	`;
	
const Form = styled.form`
	display: flex;
	flex-wrap: wrap;
	`;
	
const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 20px 10px 0px 0px;
	padding: 10px;
	`;
	
const Agreement = styled.span`
	font-size: 12px;
	margin: 20px 0px;
	`;
	
const Button = styled.button`
	width: 40%;
	border: none;
	padding:15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	`;
	
const Contain = styled.div``

const Register = () => {

	const { user } = useContext(UserContext);
	const history = useHistory();

	const [username, setUsername] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")

	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();
		// fetch('https://desolate-reaches-56234.herokuapp.com/api/users/checkEmail', {
		fetch('https://desolate-reaches-56234.herokuapp.com/api/users/checkEmail', {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email,
		    })
		})
		.then(res => res.json())
		.then(data => {
		    console.log(data);

		    if(data === true){
		    	Swal.fire({
		    		title: 'Duplicate email found',
		    		icon: 'error',
		    		text: 'Kindly provide another email to complete the registration.'	
		    	});

		    }else{
		    	// fetch('https://desolate-reaches-56234.herokuapp.com/api/users/register', {
		    	fetch('https://desolate-reaches-56234.herokuapp.com/api/users/register', {
		    		method: "POST",
		    		headers: {
		    		    'Content-Type': 'application/json'
		    		},
		    		body: JSON.stringify({
		    		    username: username,
		    		    email: email,
		    		    password: password
		    		})

		    	})
		    	.then(res => res.json())
		    	.then(data => {
		    		console.log(data);

		    		if (data === true) {

		    		    // Clear input fields
		    		    setUsername('');
		    		    setEmail('');
		    		    setPassword('');
		    		    setConfirmPassword('');

		    		    Swal.fire({
		    		        title: 'Registration successful',
		    		        icon: 'success',
		    		        text: 'Welcome to Zuitt!'
		    		    });
		    		    history.push("/login");

		    		}else{

		    		    Swal.fire({
		    		        title: 'Something wrong',
		    		        icon: 'error',
		    		        text: 'Please try again.'   
		    		    });
		    		}
		    	})
		    }
		})
	}


	useEffect(() => {
        if((username !== "" &&
			email !== "" && 
			password !== "" && 
			confirmPassword !== "") && 
			(password === confirmPassword)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [username, email, password, confirmPassword]);

	return (
		// ((user.id !== null) || (localStorage.getItem("token") !== null))
        // ?
        //     (user.isAdmin)
        //     ?
        //     <Redirect to="/login" />//admin
        //     :
        //     <Redirect to="/cart" />//shop
        // :
		// (user.email !== null) ?
		// <Redirect to="/product" />
		// :
	<Contain>
		<Header />
		<AppNavBar />
		<Container>
			<Wrapper>
				<Title>Create an account</Title>
				<Form>
					<Input placeholder="username*"/>
					<Input placeholder="e-mail*"/>
					<Input placeholder="Password*"/>
					<Input placeholder="Confirm Password*"/>
					<Agreement>
						By creating an account, I consent to the processing of my personal data in accordance with the <b>PRIVACY POLICY</b>
					</Agreement>
					<Button>
					CREATE
					</Button>
				</Form>
			</Wrapper>
		</Container>
	</Contain>
	)
}
export default Register;