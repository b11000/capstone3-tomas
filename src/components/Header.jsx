import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    height: 30px;
    background-color: teal;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 14px;
    font-weight: 500;
`

const Header = () => {
    return (
        <Container>
            Super Mega Hyper Sale!!! 1.2.2020 Free Shipping on Orders Over ₱500
        </Container>
    )
}

export default Header;
